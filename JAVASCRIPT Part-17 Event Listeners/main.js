// Event Listeners 

// syntax: addEventListener("click" function, useCaptured)

const doSomething = () => {
    alert("doing Something")
}

// h2.addEventListener("click", doSomething, false);
// h2.removeEventListener("click", doSomething, false);

// ==== arrow function ==== // 

// h2.addEventListener("click", (event) => {
//     console.log(event.target)
//     event.target.textContent = "clicked"; // new nood list 
// })

// anonymouse functuion 
// h2.addEventListener("click", function (event) {
//     console.log(event.target)
//     event.target.textContent = "clicked"; // new nood list 
// })



document.addEventListener("readystatechange", (event) => {
    if (event.target.readyState === "complete") {
        console.log("readyState: complete");
        initApp();
    }
})

const initApp = () => {
    const view = document.querySelector("#view2");
    const div = view.querySelector("div");
    const h2 = div.querySelector("h2");

    view.addEventListener("click", (event) => {
        view.style.backgroundColor = "purple"
    });


    div.addEventListener("click", (event) => {
        div.style.backgroundColor = "blue"
    });

    h2.addEventListener("click", (event) => {
        event.target.textContent = "I trust god to help all the time"
    });
}