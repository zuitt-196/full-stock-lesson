// Array 

const myArray = [];


// Add elements to an array 
myArray[0] = "Dave";
myArray[1] = 1001;
myArray[2] = false;

// Refer to an array array
console.log(myArray);


// length property 

console.log(myArray.length);

// last element in an array 
console.log(myArray[myArray.length - 1]);

// index of array 
console.log(myArray[1])

// Break the code 
/**
    push(); method --> add  the last items of array 
 */
myArray.push("School");

console.log(myArray);

// console.log(myArray[myArray.length - 1]);

// pop() method --> remove the last items of array
// myArray.pop();


let lastItem = myArray.pop();

console.log(lastItem);


// unshift() method --> is add new array in the 0 index or in the front of array


// myArray.unshift("Bong");

let newLength = myArray.unshift(42)

console.log(newLength)
console.log(myArray);

let firstItem = myArray.shift();

// console.log(firstItem);
// console.log(myArray);
// console.log(myArray[1]);

// shift() method ---> is to remove the front of array or the firts 
// 

// delete myArray[1];
// console.log(myArray[1]);


//Break the code 
/**
  splice() method ---> have differrent way to manipulate the arrray 
     delete, replace array to return new array 
 */
myArray.splice(1, 1, 45);
console.log(myArray);



const Array = ["A", "B", "C", "D", "E", "F"]

// newArray.splice(1); 

// Break the code 
/**
   slice() --> method is to cut the array start and the end 
 
 */
const newMyArray = Array.slice(2, 5);
console.log(newMyArray);


const NewString = Array.join();

const newArray = NewString.split(",");

console.log(newArray);


// Break the code 
/**
    concat() --> method is to join the array that will be result new array  


 */

const ArrayA = ["A", "B", "C"];
const ArrayB = ["E", "F", "G"]

const newArrayResult = ArrayA.concat(ArrayB);
console.log(newArrayResult);


/// Break te code 
/**
    ... -> is the spread opretor as same like the concat method 
  
 */
const newArrayRESULT = [...ArrayA, ...ArrayB];
console.log(newArrayRESULT);

const b = ["Apple", "Banana", "Santol"]
const a = ["Guava", "Durian", "Mais"]

const c = ["Ford", "Honda", "Yamaha"]
const d = ["Raider", "Xrm", "sidecar"]


console.log(a[1]);
console.log(a[0]);
console.log(a[2]);
console.log(a[1]);

a.push("Makopa")
console.log(a[3]);
console.log(a)
let addA = a.push("Lansones");
console.log(addA);
console.log(a)

const fruits = [a, b]
const things = [c, d]

console.log(fruits[0[1]])
console.log(things)