// var, let and conts 


// globa scope 

var x = 1;
let y = 2;
const z = 3;

// local scope 

// {
//     let y = 4;
//     console.log(y);

// }

// // local scope 

// function myFunc() {
//     const z = 5;
//     console.log(z);
// }


// myFunc();



function myFunc1() {
    // const z = 5;
    console.log(z);

    // local scope 

    {
        // let y = 4;
        console.log(y);

    }
}


myFunc1();




var a = 1;
let b = 2;
const c = 3;

//Global
console.log(`Global: ${a}`);
console.log(`Global: ${b}`);
console.log(`Global: ${c}`);

function myFunctin() {
    // Function 
    console.log(`Function: ${a}`);
    console.log(`Function: ${b}`);
    console.log(`Function: ${c}`);

    //Block
    {
        console.log(`Block: ${a}`);
        console.log(`Block: ${b}`);
        console.log(`Block: ${c}`);


    }
}
myFunctin()