// JSON: Javascript Object Notation  

/**
  JSON is used to send and recieve data 
  JSON is a text format that is completely language independent 
  Meaning JSON is used to send and recieve data in many language .... not juit in javasScript  
 
 */



const myObj = {
    name: "Vhong",
    hobbies: ["eat", "sleep", "code"],
    hello: function () {
        console.log("hello!");
    }
};


console.log(myObj);
console.log(myObj.name);
myObj.hello();
console.log(typeof myObj);

const sendJSON = JSON.stringify(myObj);

console.log(sendJSON);
console.log(typeof sendJSON)
console.log(sendJSON.name)

const receiveJSON = JSON.parse(sendJSON);
console.log(receiveJSON)
console.log(typeof receiveJSON)