// Loops 

// Break the code 
/**
    Create variable with value of 0 name it myNumber
    while loop have condition 
        conditiion of while loop (myNumber < 50 )
            it will return true.
            call the myNumber and initialize it  into another value which have value of 2, then use plus operator as added every loop or iterate 
    Don't create an endless loop 
  
 */

// let myNumber = 100;
// while (myNumber < 50) {
//     myNumber += 2;
//     // myNumber = myNumber + 2;
//     console.log(myNumber)
// }

// Break the code down into
/**
    do loop is only execute the one loop if the condition is true it able execute as one loop only ,
    if false it execute and loop over the condition given.
    
    the Defference between thr do and while 
     do --> loop as one 
     while --> loop over the condition given

 */
// do {
//     console.log(myNumber);
// } while (myNumber < 50) {
//     console.log("sadsa")
// }

// Breack the code 
/**
    for loop is the common method used for loop certain condition 
    inside the for loop has condiotion
    let i --> i is an integer as the variable of 0 
    i < 10 --> so it well be  return true and will execute  loop and iterate all condition 
    
 
 */
// for (let i = 0; i <= 10; i++) {
//     console.log(i);

// }

// Break the code 
/**
     1.make an variable and name it myName with value 
    2. for loop it and condition it will be has the length or index of the value myName.length which is to determine how mani index of data 
    3.console this with call the variable with charAt() method and (i) as the new value that we loop

 *  */
// let myName = "Dave";

// for (let i = 0; i < myName.length; i++) {
//     console.log(myName[i]);
//     console.log(myName.charAt((i)))
// }

// Break the code 
/**
  used by while loop 
  break keyword statements 
  contiune keyword statements 
  
    we ha to declare while condtion that return true 

 
 */
let name = "bong";
let counter = 0;
let myLater;

while (counter <= 3) {

    myLater = name[counter];
    console.log(myLater);

    if (counter === 1) {
        counter += 2;
        continue;
    }
    if (myLater === "n") break;
    counter++;
}

console.log(counter);



