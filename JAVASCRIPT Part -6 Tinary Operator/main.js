//  Conditional : Ternary Operator is know as one line code

// Syntax 
// conditional ? iftrue : iffalse; it is short had condtion


// Break  the code. Ternary is an short hand condtional 
// ? --> is symbol for asking if there have value or not / undefined or execute true 
// : -->  is execute false 

// let soup = "yes have soup today"
// let response = soup ? "yes we have soup today" : "soorry no soup today";

// console.log(response);


let soup = "Chicken noodle soup ";
let isCustomerBanned = false;

let soupRespose = isCustomerBanned ? "Soory, have soup today" : soup ? `Yes, we have ${soup} today` : "so worry no soup today";

console.log(soupRespose);


// Breack the code ternary is an short hand we able to execute the code as less code however its depends the condition 
// used datatype number we are going to initialize it  
let score = 50;
let grade = score >= 90 ? "A"
    : score > 80 ? "B"
        : score > 70 ? "C"
            : score > 60 ? "D"
                : "F";

console.log(`My test Grade is ${grade}`);


//  paper, rock scissors game 

// Break th code, Ternary is an short hand condiotional staments 
// with initialize the variable 
// using the operator and && 
// strict equal is determine if th computer/player is same value with the string "" value that assign as part of condition then the result of true value , then the return of false in Player wins

let player = "paper";
let computer = "rock";

let resultGame = player === computer ? "Tie game"
    : player === "rock" && computer === "paper" ?
        "Computer wins"
        : player === "paper" && computer === "scissors" ?
            "Computer wins"
            : "Player wins";




console.log(resultGame);