// Javascript classes ----> EA6 in 2015

// Break the code  

/**
    
    class object as almost similar from object property 
    class indicating parameter to add new object from argument but you can add wih value inside the class contructor 
 
 */

class Pizza {
    constructor(pizzaaType, pizzaSize,) {
        this.type = pizzaaType;
        this.size = pizzaSize;
        this.crust = "original";
        //  making the object array
        this.toppings = [];
    }
    // used to get keyword to access the object 
    // get pizzaSizeCrust() {
    //     return this.crust;
    // }

    // we able also to make our own method as get the object 
    getCrust() {
        return this.crust;
    }

    // wa able also to make our own method as set from getCrust method
    settCrust(pizzaSizeCrust) {
        this.crust = pizzaSizeCrust;
    }
    // uses set keyword to set the get keyword then setter the object  
    // set pizzaSizeCrust(pizzaSizeCrust) {
    //     this.crust = pizzaSizeCrust;
    // }

    // get the object or data from empty array 
    gettoppings() {
        return this.toppings;
    }

    // set the object or data from  gettoppings() and used to push or add new list of array
    setToppings(toppings) {
        this.toppings.push(toppings);
    }

    bake() {
        console.log(`Baking a ${this.size}  ${this.type} ${this.crust} crust pizza.`);
    }
}

// Create Object

const Mypizza = new Pizza("Huawaian", "Small");
// Mypiza.type = "newPepperoni"; // --> it not goo deseireable let to prevent this happen to conviction 
// Mypizza.bake();

// must be same method to access the get keyword

// Mypizza.pizzaSizeCrust = "sausage";
Mypizza.settCrust("thin")
// we able to access the object directly used  with dot natation as the common way to access the object 


//list of array  elements from setToppings 
Mypizza.setToppings("orange");
Mypizza.setToppings("chocolate");
Mypizza.setToppings("milk");
Mypizza.setToppings("pinapple");
Mypizza.bake();


console.log(Mypizza.getCrust());

// console the gettooppign method
console.log(Mypizza.gettoppings());



// parent class is a blueprint 

// break the code 

/**
  we create generic class or the parent class used as the blueprint 
 we are not intiatiate the object to make variable for newPizzaa 
  
 
 */
class NewPizaa {
    constructor(PizzaSize) {
        this.size = PizzaSize;
        this.crust = "original";
    }

    getCrust() {
        return this.crust;
    }
    setcrust(PizaaCrust) {
        this.crust = PizaaCrust;
    }
}


// child class that connect to parent  used extends key 
/**
  Break the code 
    Creating a child class from the parent NewPizaa 
        extends -->  is key to extent more the object 
        super -->  Inside the child class constructor it shuold be used (super) key to pass the constructor in parent their parameter 
        the we code the type property with this - keyword.
        And make make own method slice() 
 */

class specialtyPizza extends NewPizaa {
    constructor(pizzaSize) {
        super(pizzaSize);
        this.type = "The Works";
    }
    slice() {
        console.log(`Our ${this.type} ${this.size} pizza has 8 slices.`)
    }
}


// intiatiate the object mySpecialty as the value specialtyPizza
const mySpecialty = new specialtyPizza("midium");
// invoke the  intiatiate object with own slice method
mySpecialty.slice();


// make own simpel blueprint


// this undescore is to intend to private  support the private failed 
// class anotherPizaa {
//     constructor(PizzaSize) {
//         this._size = PizzaSize;
//         this._crust = "original";
//     }

//     getCrust() {
//         return this._crust;
//     }
//     setcrust(PizaaCrust) {
//         this._crust = PizaaCrust;
//     }
// }




// factory Function 
/**
    It it a priavet never acces the property or failed or the naming conviction inside ther Factory function 
 */

function pizzaFactor(pizzaSize) {
    const crust = "original";
    const size = pizzaSize;

    return {
        bake: () => console.log(`Bake a ${size} ${crust} crust pizza`)
    };
}

const myPizza = pizzaFactor("small");
myPizza.bake();









class anotherPizza {
    crust = "original";
    #sauce = "tradiotional";
    #size;
    constructor(PizzaSize) {
        this.#size = PizzaSize;

    }

    getCrust() {
        return this.crust;

    }
    setcrust(PizaaCrust) {
        this._crust = PizaaCrust;
    }

    hereYouGo() {
        console.log(`Here's you ${this.crust} ${this.#sauce} sauce ${this.#size} pizza...`);
    }
}


const myNewizza = new anotherPizza("large");

myNewizza.hereYouGo();

console.log(myNewizza.getCrust()); /// --> is an  public
console.log(myNewizza.sauce); /// --> is an private 