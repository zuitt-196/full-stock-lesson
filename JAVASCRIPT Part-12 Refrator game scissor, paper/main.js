// Rock, paper, scissor: Refractores with function 

const initGame = () => {
    const startGame = confirm("shall we play rock, paper, or scissor");
    startGame ? playGame() : alert("Ok, maybe next time");
}

// Game flow function 

const playGame = () => {
    while (true) {
        let playerChoice = getPlayerChoice();
        playerChoice = formatPlayerChoice(playerChoice);
        if (playerChoice === "") {
            invalidChoice();
            continue;
        }
        if (!playerChoice) {
            decideNotToplay();
            break;
        }
        playerChoice = evaluatePlayerChoice(playerChoice);
        if (!playerChoice) {
            invalidChoice();
            continue;
        }

        const computerChoice = getcomputerChoice();
        const result = determineWinner(playerChoice, computerChoice);
        displayResult(result);
        if (askingPlayAgain()) {
            continue;
        } else {
            thanksPlaying();
            break;
        }





    }
};




// global function 

// =====[GET PLAYER CHOICE] ===== // 
const getPlayerChoice = () => {
    return prompt("Please enter rock, paper, or scissor")
};

// ===== [FORMAT PLAYER CHOICE] ==== // 
const formatPlayerChoice = (playerChoice) => {
    if (playerChoice || playerChoice === "") {
        return playerChoice.trim().toLowerCase();

    } else {
        return false;
    }
};


// ===== [INVALID CHOICE ==== // 
const invalidChoice = () => {
    alert("You didn't enter rock, paper, or scissor");
}


// ==== [DECIDE NOT TO PAY ] ===== // 
const decideNotToplay = () => {
    alert("I guess you changed your mind. maybe next time.");
};

// ===== [EVALAUTE THE PLAYER CHOICE] ==== //  
const evaluatePlayerChoice = (playerChoice) => {
    if (playerChoice === "rock" || playerChoice === "paper" || playerChoice === "scissor") {
        return playerChoice;

    } else {
        return false;
    }
}

// ===== [COMPUTER CHOICE] ====  //

const getcomputerChoice = () => {
    const randomChoice = Math.floor(Math.random() * 3);
    const rpsArray = ["rock", "paper", "scissor"]
    return rpsArray[randomChoice];
}

// ==== [DETERMINE WINNER] === //

const determineWinner = (player, computer) => {
    // Ternary condition 
    const winner =
        player === computer
            ? "Tie game"
            : player === "rock" && computer === "paper"
                ? `PlayerOne ${player}\nComputer ${computer}\n Computer wins!`
                : player === "paper" && computer === "rock"
                    ? `PlayerOne ${player}\nComputer ${computer}\n Computer wins!`
                    : player === "scissors" && computer === "rock"
                        ? `PlayerOne ${player}\nComputer ${computer}\n Computer wins!` :
                        `PlayerOne ${player}\nComputer ${computer}\n Computer wins!`;

    return winner;
}

// ==== [DISPLAY RESULTS] === //

const displayResult = (result) => {
    alert(result);
};

//  === [ASLKING PLAY AGAIN ] ==== //
const askingPlayAgain = () => {
    return confirm('Play Again');
}

// === [THANKS FOR PLAYING ] === // 
const thanksPlaying = () => {
    alert("Ok, thaks for playing")
}
initGame();