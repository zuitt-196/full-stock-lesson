// Objects
// key-value pairs in curly brces 

// Breack the code 
/**
    Object is common use for storing the data. consist of key-value within the curly braces 
    
    object it can be store what are the data type that we need to store 
    key and thh value of the value 

    declaration of method inside the object 

    action --> is an method with anonymous function 
  
 
 */
const myObj = { name: "Dave" }

const anotherObj = {
    alive: true,
    anwser: 42,
    hobbies: ["Eat", "sleep", "code"],
    beverage: {
        morning: "coffe",
        afternoon: "Iced Tea"
    },
    // declare a method with anonymous function
    action: function () {
        // return "helloe word";
        return `Time for ${this.beverage.morning}`;
    }

}

// calling the object 
/*
    . dot notation ---> is used for access the list of key value 
    [] sqaure bracket --> is allow us to acced the list of key value usd the quote 


*/

// Break the code 
/**
    inhiritance --> is the way to inherit from the generic object as to create new object 
    
    Object.create() ---> is to create new object 
 
 */


console.log(anotherObj.hobbies[0])
console.log(anotherObj["alive"])
console.log(anotherObj.anwser)
console.log(anotherObj.beverage.morning)





const vehicle = {
    wheels: 4,
    Brand: "Toyota",
    engine: function () {
        return "Brommmmm";
    }
}

const car = Object.create(vehicle); // vwi-cle contructor 
car.doors = 4;  /// ---> dot notation is to  acces the index of object 
car.brand = "Ford";
car.model = "M45";
car.engine = function () {
    return "whossssss";
}
console.log(car.Brand)
// car.engine = function () {
//     return "whoosh";
// };
// console.log(car.engine());

// console.log(car.engine());


const tesla = Object.create(car);
tesla.engine = function () {
    return "shhhhh"
}
console.log(tesla.wheels);
console.log(tesla.engine());



const band = {
    vocal: "Robert Plant",
    guitar: "Jimmy Page",
    bass: "Jonh Paul Jones"



}


// Breack the code

/**
    Object.key() method -->  is to define the method of Object 
    Object.values() method ---> is to define the value of key property
    hasOwnProperty() method --- > is to knowig is existing or not in object property 

 */

delete band.drums;
console.log(band.hasOwnProperty('drums'));
console.log(band);

console.log(Object.keys(band));
console.log(Object.values(band));


for (let job in band) {
    // console.log(`On ${job}, it's ${band[job]}!`);
    // console.log(job)
    // console.log(band[job]) /// ----> list of value of keys
    // console.log(band)
    // console.log(job)
    // console.log(band[job])
    console.log(`On ${job}., it's ${band[job]}!`)


}


// destructing objects 

// Break the code 
/**
   It almost same from contruction object 



 */
const { guitar: myVariable1, drums: myVariable2 } = band;
console.log(myVariable1);
console.log(myVariable2);


const { vocal, guitar, bass, drums } = band;
console.log(vocal);
console.log(guitar);
console.log(bass);
console.log(drums);


function sings({ vocal, }) {
    return `${vocal} sings!`;
}

console.log(sings(band));