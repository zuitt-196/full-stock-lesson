// JavasScipt Errors and Error Handling 

"Use Strict"


// Break the code 
/**
    
 
 */
// const makeError = () => {
//     try {
//         // const name = "Dave";
//         // name = "joe"
//         throw new customError("This is a custom error!");


//     } catch (err) {
//         console.error(err.message);
//         console.error(err.name);
//         console.error(err.stack)
//     }
// }

// makeError();



// function customError(message) {
//     this.message = message;
//     this.name = "customError";
//     this.saying = "Pagtarong og code oieeee!!!"
//     this.stack = `${this.name}: ${this.message} ${this.saying}`;
// }



// Generic Error Handler

const makeAnotherError = () => {
    let i = 1;
    while (i <= 5) {
        console.log(i);
        try {
            if (i % 2 !== 0) {
                throw new Error("Odd number")
            }
            console.log("Even number")


        } catch (err) {
            console.error(err.stack)

        } finally {
            console.log("...Finally has no remainsder")
            i++;

        }
    }

}


makeAnotherError();

