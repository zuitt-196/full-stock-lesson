// DOM - Documnent Object Model


// GET BY id
const view1 = document.getElementById('view1');
console.log(view1);

//GET  BY Selector 
const view2 = document.querySelector("#view2");
console.log(view2);

view1.style.display = "flex";
view2.style.display = "none"

// GET BY Class Name get --> HTMLcollection 
const views = document.getElementsByClassName("view1");
console.log(views);

// GET BY Class Select all class --> NodeList 
const sameViews = document.querySelectorAll(".view");
console.log(sameViews);

//  GET  the tag Name of view1 as the value of Id 
const divs = view1.querySelectorAll("div");  /// ----> nodelist of div tag
console.log(divs);


const sameDivs = view1.getElementsByTagName("div"); /// ---> HTML Collection 
console.log(sameDivs);


const eveDivs = view1.querySelectorAll("div:nth-of-type(2n)");/// -->  to select the even div used by nth-of-type(2n)


// loop the even 

for (let i = 0; i < eveDivs.length; i++) {
    // const element = eveDivs[i];
    // element.style.backgroundColor = "darkblue";
    // element.style.width = "200px";
    // element.style.height = "200px";

    eveDivs[i].style.backgroundColor = "darkblue";


}

// Basci multilpication table
// let x = 0;
// for (let x = 0; x <= 10; x++) {
//     const i = x;
//     const multilpy = i * 5;
//     const result = multilpy * i;
//     console.log(`${i} * ${multilpy} = ${result}`);
// }

const navText = document.querySelector("nav h1");
console.log(navText);
navText.textContent = "Hello world"; // --> set or specified node 

const navBar = document.querySelector("nav");
navBar.innerHTML = `<h1>Hello!</h1> <p> This should align right</p>`;

console.log(navBar);

navBar.style.justifyContent = "space-between";

console.log(eveDivs[0]);

// GET THE PARENT  element how  to navigate the nodelist and HTML collection 

console.log(eveDivs[0].parentElement)// exampl ul tag 
console.log(eveDivs[0].parentElement.children); // --> li tag HTML collection 
console.log(eveDivs[0].parentElement.childNodes); // --> Nodelist with div and text 
console.log(eveDivs[0].parentElement.hasChildNodes); // --> true
console.log(eveDivs[0].parentElement.lastChild); // --> the last child
console.log(eveDivs[0].parentElement.lastElementChild); // the last property of the parent with thier value 
console.log(eveDivs[0].parentElement.firstElementChild); // the first property  of the parent  
console.log(eveDivs[0].nextElementSibling.nextElementSibling); // from the eve value object eveDivs has been locate from odd since there are first of the child 
console.log(eveDivs[0].previousElementSibling); /// ---> alsmost same from firstElementChild 
console.log(eveDivs[0].previousSibling) // navigate the node list


// Break the code 

/**
    For applying and accessing the element. They are should be inside the document or DOM  
    Ex. const name = documment.querySelector(".div")

    With adding some style it must be have used dot notation to accesss and specified the certain object 
    Ex.view1.style.display = "none"; 
 
 */

view1.style.display = "none";
view2.style.display = "flex";
view2.style.flexDirection = "row";
view2.style.flexWrap = "wrap";
view2.style.backgroundColor = "darkblue";


// Intances how to clear the page or certain element inside the browser 
// used while loop to remove the element in pagem, but this diserable 
while (view2.lastChild) {
    view2.lastChild.remove();

}


// To create Element or tag used arrow function 
/**
    Used by arrow functions with parrameter(parent, iter)
    parent --> append from the view2 whereas element already, but sad to say it was removed for creating new Div purposes
    iter --> as the number uses by the textContent

    intintiate new variable --> with value of document.createElement("div");

    append the parent by newDiv
    append() -->method inserts a set of Node objects or string objects after the last child of the Element. String objects are inserted as equivalent Text nodes.



 * 
 */

const crateDivs = (parent, iter) => {
    const newDiv = document.createElement("div");
    newDiv.textContent = iter;
    newDiv.style.backgroundColor = "#000";
    newDiv.style.width = "100px";
    newDiv.style.height = "100px";
    newDiv.style.margin = "10px";
    newDiv.style.display = "flex";
    newDiv.style.justifyContent = "center";
    newDiv.style.alignItems = "center";
    parent.append(newDiv); // ---> append or inserts node object 


}

// crateDivs(view2, 10);

//  loop the crateDivs

// loop the  crateDivs into 12 div  
for (let i = 1; i <= 12; i++) {
    // const newDiv = crateDivs(view2, i);
    crateDivs(view2, i);



}

